/*eslint-env browser*/

// Variables
var overlay = gEClass('overlay')[0];
var deleteBtns = gEClass('delete-service');
var yes = gEId('yes');
var no = gEId('no');
var url = yes.getAttribute('href');

// Functions
function confirmation() {
	var id = this.getAttribute('data-id');
	var deleteUrl = url + id;
	yes.setAttribute('href', deleteUrl);
	overlay.classList.add('visible');
}

function cancel() {
	overlay.classList.remove('visible');
}

// Event listeners
for (let i = 0; i < deleteBtns.length; i++) {
	deleteBtns[i].addEventListener('click', confirmation);
}

no.addEventListener('click', cancel);
