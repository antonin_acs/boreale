/*eslint-env browser*/

// Variables
var title = gEName('title')[0];
var desc = gEName('description')[0];
var price = gEName('price')[0];
var pic = gEName('picture')[0];

var submitBtn = gEName('submit-service')[0];

var errors = 0;
var errorTitle = '';

// Functions

function submitService(e) {
	e.preventDefault();

	var addService = gEId('add-service');
	// Check if on service addition page
	if (document.body.contains(addService)) {

		var formData = new FormData();

		formData.append('title', title.value);

		var request;
		if (window.XMLHttpRequest) {
			request = new XMLHttpRequest();
		}
		else {
			request = new ActiveXObject('Microsoft.XMLHTTP');
		}

		// if on service addition page, check if the name is taken
		// and then check form
		request.open('POST', '/verification-titre', true);
		request.setRequestHeader('X-CSRF-TOKEN', getToken());
	  request.responseType = 'text';
	  request.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) {
	      var response = this.responseText;
	      errors = 0;
	      errorTitle = '';
	      if (response === '1') {
	      	errors++;
	      	errorTitle = 'Ce titre est déjà utilisé';
	      	gEId('error-title').innerHTML = errorTitle;
	      }
	      checkForm();
	    }
	  }
	  request.send(formData);

	  // if not on service addition page, directly check form
	} else {
		checkForm();
	}
}

function checkForm() {

	if (title.value.length === 0) {
		errors++;
		errorTitle = 'Veuillez renseigner un titre de prestation.';
	}
	gEId('error-title').innerHTML = errorTitle;

	var errorDesc = '';
	if (desc.value.length === 0) {
		errors++;
		errorDesc = 'Veuillez renseigner une description.';
	}
	gEId('error-desc').innerHTML = errorDesc;

	var errorPrice = '';
	if (price.value.length === 0) {
		errors++;
		errorPrice = 'Veuillez renseigner un prix.';
	}
	gEId('error-price').innerHTML = errorPrice;

	if (errors === 0) {
		gEId('service-form').submit();
	}
}

// Event listener
submitBtn.addEventListener('click', submitService);
