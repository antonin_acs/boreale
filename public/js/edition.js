/*eslint-env browser*/

//variables

var num = '', i, group, icon;
var quantity = gEClass('edit-icon').length;
var savedTexts = new Array(quantity);

// get current page
var path = document.location.href;
var page = path.substring(path.lastIndexOf( '/' )+1);

if (page == '')
	page = 'home';

// functions

function sendNewText(element) {
	var request;
  if (window.XMLHttpRequest)
		request = new XMLHttpRequest();
  else
    request = new ActiveXObject('Microsoft.XMLHTTP');

  request.open('POST', '/update-text', true);
  request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	request.setRequestHeader('X-CSRF-TOKEN', getToken());
  request.responseType = 'text';
  var data =
    'data-text='+element.value+'&data-page='+page+'&data-title='+element.getAttribute('data-title');

  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
			var answer = this.responseText;
			gEClass('editable')[num].innerHTML = answer;
    }
		else if (this.readyState == 4 && this.status != 200) {
			alert('erreur '+this.status);
    }
  }
  request.send(data);
}


// events listeners

//edition icons

for (i = 0; i < quantity; i++) {
	gEClass('edit-icon')[i].addEventListener('click', function() {
		num = (this.id).substring(9);
		group = 'edit-group'+num;
		group = gEId(group);
		gEClass('editable')[num].innerHTML = gETag('textarea')[num].value;
		group.classList.remove('hidden');
		this.classList.add('hidden');
		gEClass('editable')[num].classList.add('hidden');
	});
}

//cancel

for (i = 0; i < quantity; i++) {
	gEClass('edit-cancel')[i].addEventListener('click', function() {
		gETag('textarea')[num].value = gEClass('editable')[num].innerHTML;
		num = (this.id).substring(11);
		group = 'edit-group'+num;
		group = gEId(group);
		group.classList.add('hidden');
		gEClass('edit-icon')[num].classList.remove('hidden');
		gEClass('editable')[num].classList.remove('hidden');
	});
}

//valid

for (i = 0; i < quantity; i++) {
	gEClass('edit-valid')[i].addEventListener('click', function() {
		num = (this.id).substring(10);
		group = 'edit-group'+num;
		group = gEId(group);
		group.classList.add('hidden');
		gEClass('edit-icon')[num].classList.remove('hidden');
		gEClass('editable')[num].classList.remove('hidden');
		sendNewText(gETag('textarea')[num]);
	});
}

