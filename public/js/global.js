/*eslint-env browser*/

// Variables

var burger = gEClass('burger')[0];
var bMenu = gEClass('burger-menu')[0];
var userIcon = gEId('user-icon');
var userMenu = gEClass('user-menu')[0];

// Functions

function gEId(E) {
	return document.getElementById(E);
}

function gEClass(E) {
	return document.getElementsByClassName(E);
}

function gEName(E) {
	return document.getElementsByName(E);
}

function gETag(E) {
	return document.getElementsByTagName(E);
}

function getToken() {
    var meta = gEName('csrf-token')[0];
    return meta.getAttribute('content');
}

function openBurger() {
	burger.classList.add('active');
	bMenu.classList.add('visible');
}

function closeBurger() {
	burger.classList.remove('active');
	bMenu.classList.remove('visible');
}

function openUser() {
	userMenu.classList.add('visible');
}

function closeUser() {
	userMenu.classList.remove('visible');
}

// Event listeners

burger.addEventListener('click', function() {
	if (burger.classList.contains('active')) {
		closeBurger();
	}
	else {
		closeUser();
		openBurger();
	}
});

if (document.body.contains(userIcon)) {
	userIcon.addEventListener('click', function() {
		if (userMenu.classList.contains('visible')) {
			closeUser();
		}
		else {
			closeBurger();
			openUser();
		}
	});
}

window.addEventListener('resize', function() {
	closeBurger();
	closeUser();
});