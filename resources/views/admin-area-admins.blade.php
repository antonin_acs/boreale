@extends('default')

@section('title')
	Espace administrateur
@endsection

@section('content')
<main id="admin-admins">
    <div id="user-header">
        <p>Bonjour, {{ session('fname') }} </p>
    </div>
</main>

@foreach ($result as $admin)
<section class="admin-display">
	<p class="editable">{{$admin -> name}} {{$admin -> firstname}}</p>
	@if($admin -> id_admin != 1)
	<a href="{{('/admin-delete?id=')}}{{$admin -> id_admin}}"><button class="inpBtn" id="delete-button" name="delete-button">{{__('Supprimer l\'admin')}}</button></a>
	@endif
</section>
@endforeach

<a href="{{url('/espace-admin/creation-admin')}}"><button class="inpBtn">{{__('Créer un compte admin')}}</button></a>

@endsection
