@extends('default')

@section('title')
	Espace patient(e)
@endsection

@section('content')
<main id="patient">
	<div id="user-header">
		<p>Bonjour, <span>{{ session('fname') }}</span></p>
	</div>

	<q class="editable">{{ $texts[0] }}</q>
	@if (session('type') == 'admin')
		<div id="edit-group0" class="hidden edit-area">
			<textarea class="edit-textarea" name="edit-area0" data-title="0">{{ $texts[0] }}</textarea>
			<button type="button" id='edit-cancel0' class="edit-cancel">annuler</button>
			<button type="button" id="edit-valid0" class="edit-valid">valider</button>
		</div>
		<div id="edit-icon0" class="edit-icon"></div>
	@endif

	<nav>
		<a href="{{ url('/espace-patient/recherche') }}" class="nav-area-btn">Rechercher des prestations</a>
		<a href="{{ url('/espace-patient/agenda') }}" class="nav-area-btn">Acc&eacute;der &agrave; mon agenda</a>
		<a href="{{ url('/espace-patient/profil') }}" class="nav-area-btn">G&eacute;rer mon profil</a>
	</nav>

</main>
@endsection
