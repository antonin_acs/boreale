@extends('default')

@section('title')
	Edition d'une prestation
@endsection

@section('content')
<main id="edit-service">

    <div id="user-header">
        <p>Bonjour, {{ session('fname') }} </p>
    </div>

	<h2>Edition de prestation</h2>
	<form method="post" action="{{ url('espace-admin/prestations/maj') }}" enctype="multipart/form-data" id="service-form">
		@csrf
		<input type="hidden" name="id" value={{ $service -> id_service }}>
		<input type="text" name="title" value="{!! $service -> title !!}">
		<span id="error-title"></span>
		<textarea name="description">{!! $service -> description !!}</textarea>
		<span id="error-desc"></span>
		<input type="text" name="price" value="{!! $service -> price !!}">
		<span id="error-price"></span>
		<input type="file" name="picture">

		<input type="submit" name="submit-service">
	</form>
</main>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('/js/service-validation.js') }}"></script>
@endsection
