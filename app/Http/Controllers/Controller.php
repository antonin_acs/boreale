<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Redirector;
use App\Http\Controllers\Controller;

require('functions.php');
require('Model.php');

class Controller extends BaseController {
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function home() {
//		session()->flush();
		$texts = getTexts('home');
		return view('home', ['texts' => $texts]);
	}

	public function signin() {
		if(session('user')){
			return redirect()->action('Controller@home');
		}
		$advice = '';
		if (isset($_GET['signin-button'])) {
			$email = $_GET['email'];
			$pass = $_GET['password'];

			$account = login($email, $pass);
			//if connection succeeded
			if ($account !== false) {
				$accountType = $account['type'];
				$fname = $account['fname'];
				$super = $account['super'];
				session(['session' => true, 'user' => $email, 'type' => $accountType, 'fname' => $fname, 'super' => $super]);

			//redirect toward appropriate page
			if ($accountType === 'patient')
				return redirect()->action('PatientController@patientArea');
			if ($accountType === 'practitioner')
				return redirect()->action('PractitionerController@practitionerArea');
			if ($accountType === 'admin')
				return redirect()->action('AdminController@adminArea');
			}
			$advice  = 'Adresse mail ou mot de passe erroné';
		}
		return view('signin', ['advice' => $advice]);
	}

	public function signup() {
		if(session('user')){
			return redirect()->action('Controller@home');
		}
		$popup = false;
		return view('signup', ['popup' => $popup]);
	}

	public function signout() {
		session()->flush();

		return redirect()->action('Controller@home');
	}

	public function forgotPassword() {

		if ($_SERVER['REQUEST_METHOD'] !== 'POST') {  // first time ... (or if user try to change request method)
			$popup = false;
		}
		else {  // ... and after submit
			$mail = Input::get('mail');
			$random = generateLink();

			$recover = recoverPassByMail($mail, $random);
			if ($recover) {

				$baseMail = getMail('lost-password');
				$message = $baseMail['message'];

				$link = './oubli/'.$random;

				$message = '<html><body>'.$message
										.'<a href="'.$link.'">
												<button style="padding:6px 10px;font-size:18px;color:#EEE;background-color:#864;">
												Changer mon mot de passe</button>
											</a>
										</body></html>';

				$message = wordwrap($message, 70);
				$subject = $baseMail['object'];

				$headers = "From: " .'cantin.p@codeur.online'. "\r\n";
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

				//send mail
				mail($mail, $subject, $message, $headers);

				$popup = 'Un mail vous a été envoyé afin de réinitialiser votre mot de passe';
			}
			else
				$popup = 'une erreur est survenue';

			return view('forgot-password', ['popup' => $popup]);
		}
		return view('forgot-password', ['popup' => $popup]);
	}

	public function changePassword($link) {

		if ($_SERVER['REQUEST_METHOD'] !== 'POST') { 	// first time ... (or if user try to change request method)
			$popup = false;
			$link = filter_var($link, FILTER_SANITIZE_STRING);
			if (testLink($link) === false)
				abort(404);
		}
		else {  // ... and after submit

			$password = Input::get('password');

			// sanitize and hash new pass
			$password = filter_var($password, FILTER_SANITIZE_STRING);
			$password = password_hash($password, PASSWORD_BCRYPT);

			modifyPassword($link, $password);
			$popup = 'Votre mot de passe a été modifié avec succès';
		}

		return view('change-password', ['popup' => $popup, 'link' => $link]);
	}

	public function existEmail() {

		if (isset($_POST['data-mail'])) {
			$mail = filter_var($_POST['data-mail'], FILTER_SANITIZE_STRING);
			$exist = exist($mail);
			$exist = ($exist) ? 'true' : 'false';
			return $exist;
		}
	}

	public function patientInfos() {
		$texts = getTexts('patient-infos');
		return view('patient-infos', ['texts' => $texts]);
	}

	public function practitionerInfos() {
		$texts = getTexts('practitioner-infos');
		return view('practitioner-infos', ['texts' => $texts]);
	}

	public function practitionerServices() {
		if (session('type') === 'practitioner'){
			$texts = getTexts('practitioner-services');
			$services = services();
			$mail = session('user');
			$id_practitioner = idPractitioner($mail)->id_practitioner;
			$have_services = haveServices($id_practitioner);

			return view('practitioner-services', ['services' => $services,
				'have_services' => $have_services, 'texts' => $texts]);
		}
		else if(session('type') === 'admin') {
			$texts = getTexts('practitioner-services');
			return view('practitioner-services', ['texts' => $texts]);
		}
		abort(403, 'Vous n\'avez pas le rang nécessaire pour voir cette page !');
	}

	public function existTitle() {
		$title = Input::get('title');
		$exist = serviceTitle($title);

		return (string)$exist;
	}

	public function toggleMail() {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {

			$type = Input::get('data-type');

			// sanitize data
			$type = filter_var($type, FILTER_SANITIZE_STRING);

			$mail = getMail($type);
			$mail = implode('?%%', $mail);
			return $mail;
		}
	}

	public function updateMail() {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {

			$type = Input::get('data-type');
			$object = Input::get('data-object');
			$message = Input::get('data-message');

			// sanitize data
			$type = filter_var($type, FILTER_SANITIZE_STRING);
			$object = filter_var($object, FILTER_SANITIZE_STRING);
			$message = filter_var($message, FILTER_SANITIZE_STRING);

			mailUpdate($type, $object, $message);
		}
	}

	public function areaProfil() {

		// if user is logged
		if (session('user')) {

			// get informations depending of account type
			$account = informations(session('user'), session('type'));
			$texts = getTexts('profile');

			$name = $account['name'];
			$fname = $account['fname'];
			$email = $account['email'];

			// patients & practitioners
			if (session('type') !== 'admin') {
				$address = $account['address'];
				$zip = $account['zip'];
				$city = $account['city'];

				// practitioners
				if (session('type') === 'practitioner') {
					$radius = $account['radius'];
					$siret = $account['siret'];
					$iban = $account['iban'];

					// practitioners
					return view('area-profil', ['texts' => $texts, 'fname' => $fname, 'name' => $name, 'address' => $address, 'zip' => $zip, 'city' => $city, 'radius' => $radius, 'siret' => $siret, 'iban' => $iban, 'email' => $email]);
				}
			// patients
			return view('area-profil', ['texts' => $texts, 'fname' => $fname, 'name' => $name, 'address' => $address, 'zip' => $zip, 'city' => $city, 'email' => $email]);
			}
			else {
				// admins
				return view('area-profil', ['texts' => $texts, 'fname' => $fname, 'name' => $name, 'email' => $email]);
			}
		}
		abort(403, 'Vous n\'avez pas le rang nécessaire pour voir cette page !');
	}

	public function agenda() {
		if (session('user')) {
			$texts = getTexts('agenda');
			return view('agenda', ['texts' => $texts, 'id' => false]);
		}

		abort(403); // if user isn't logged in
	}

	public function contact() {
		$texts = getTexts('contact');
		return view('contact', ['texts' => $texts]);
	}

	public function aboutUs() {
		$texts = getTexts('about-us');
		return view('about-us', ['texts' => $texts]);
	}

	public function legal() {
		$texts = getTexts('legal');
		return view('legal-notice', ['texts' => $texts]);
	}

	public function partner() {
		$texts = getTexts('partner');
		return view('partner', ['texts' => $texts]);
	}

	public function faq() {
		$texts = getTexts('faq');
		return view('faq', ['texts' => $texts]);
	}

	public function updatePsw(){
		$password = Input::get('password');
		$npassword = Input::get('new-password');
		$cpassword = Input::get('confirm-password');
		$email = session('user');

		$errorMsg = [];
		$errors = 0;

		$oldPass = verifyPassword($email);

		if (password_verify($password, $oldPass)) {

			if($npassword === $password){
				$errors++;
				array_push($errorMsg, 'Rentrez un mot de passe différent de l\'ancien.');
			}
			if ($npassword !== $cpassword) {
				$errors++;
				array_push($errorMsg, 'Vos mots de passe ne sont pas identiques.');
			}
			if (!preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$-.:-?{-~!"^_`\[\]\/])(?=.{8,})/',
				$cpassword)) {
				$errors++;
				array_push($errorMsg, 'Votre mot de passe doit contenir au moins 8
					caractères, dont une majuscule, une minuscule, un chiffre et un
					symbole.');
			}

			$password = filter_var($password);
			$password = password_hash($password, PASSWORD_BCRYPT);
			$npassword = filter_var($npassword);
			$npassword = password_hash($npassword, PASSWORD_BCRYPT);
			$cpassword = filter_var($cpassword);
			$cpassword = password_hash($cpassword, PASSWORD_BCRYPT);

			$type = session('type');

		} else {
			$errors++;
			array_push($errorMsg, 'Ancien mdp mauvais');
		}

		if ($errors !== 0) {
			return view('error', ['errors' => $errorMsg]);
		}
		else{
			updatePsw($type, $email, $cpassword);
			if ($type === 'patient')
				return redirect()->to('/espace-patient/profil')->send();
			if ($type === 'practitioner')
				return redirect()->to('/espace-praticien/profil')->send();
			if ($type === 'admin')
				return redirect()->to('/espace-admin/profil')->send();
		}
	}

	public function ajaxUpdatePsw(){
		$password = Input::get('data-pass');
		$email = session('user');

		$oldPass = verifyPassword($email);

		if (password_verify($password, $oldPass)) {
			return 'true';
		}
		else {
			return 'false';
		}
	}

	public function updateInfo(Request $request) {

		if ($_SERVER['REQUEST_METHOD'] === 'POST'){
			$name = Input::get('name');
			$fname = Input::get('fname');
			$email = Input::get('email');
			$address = Input::get('address');
			$zip = Input::get('zip');
			$city = Input::get('city');
			$accountType = session('type');

			$name = filter_var($name, FILTER_SANITIZE_STRING);
			$fname = filter_var($fname, FILTER_SANITIZE_STRING);
			$email = filter_var($email, FILTER_SANITIZE_STRING);
			$address = filter_var($address, FILTER_SANITIZE_STRING);
			$zip = filter_var($zip, FILTER_SANITIZE_STRING);
			$city = filter_var($city, FILTER_SANITIZE_STRING);


			$errorMsg = [];
			$errors = 0;


			if (!preg_match('/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i', $email)) {
				$errors++;
				array_push($errorMsg, 'Adresse mail invalide.');
			}
			elseif ($email !== session('user')) {
				$exist = exist($email);
				if ($exist === true) {
					$errors++;
					array_push($errorMsg, 'L\adresse mail saisie existe déjà');
				}
			}

			if (!preg_match('/^(?=.*[A-Z])(?=.{2,})/i', $fname) ||
				strlen($fname) > 50) {
				$errors++;
				array_push($errorMsg, 'Format de prénom invalide.');
			}
			if (!preg_match('/^(?=.*[A-Z])(?=.{2,})/i', $name) ||
				strlen($name) > 50) {
				$errors++;
				array_push($errorMsg, 'Format de nom invalide.');
			}

			$radius = 0;
			$siret = 0;
			$iban = 0;

			if (session('type') !== 'admin' ) {

				if (!preg_match('/^(?=.*[A-Z])(?=.{2,})/i', $address)) {
					$errors++;
					array_push($errorMsg, 'Format d\'adresse invalide.');
				}
				if (!preg_match('/^[0-9]{5}$/i', $zip)) {
					$errors++;
					array_push($errorMsg, 'Code postal invalide.');
				}
			}

			if (session('type') === 'practitioner') {

				$radius = Input::get('radius');
				$siret = Input::get('siret');
				$iban = Input::get('iban');

				$iban = preg_replace('/\s+/', '', $iban);
				$siret = preg_replace('/\s+/', '', $siret);

				$radius = filter_var($radius, FILTER_SANITIZE_STRING);
				$iban = filter_var($iban, FILTER_SANITIZE_STRING);
				$siret = filter_var($siret, FILTER_SANITIZE_STRING);

				if(verifiateIban($iban) !=1 || !preg_match('/^[A-Z0-9]{14,35}$/i', $iban)){
					$errors++;
					array_push($errorMsg, 'Numéro Iban invalide.');
				}

				if($radius > 250 || $radius < 10 || !preg_match('/^[0-9]{2,3}$/i', $radius)){
					$errors++;
					array_push($errorMsg, 'Rayon d\'action non autorisé.');
				}

				if (isSiret($siret) != true){
					$errors++;
					array_push($errorMsg, 'Numéro de SIRET invalide.');
//					return isSiret($siret)[0];
				}

				$dir = 'img/services/';
				$pic_name = '';

				if ($request->hasFile('file')) {
					$pic_name = $request->file('file')->getClientOriginalName();
					$pic = $request->file('file');

					$info = getimagesize($pic);
					if ($info !== FALSE) {
						move_uploaded_file($pic, $dir.$pic_name);
					} else {
						array_push($errorMsg, 'Le fichier n\'est pas une image.');
						$errors++;
					}
				}
			}

			if ($errors === 0) {
				$type = session('type');
				$oldmail = session('user');
				infoUpdate($type, $fname, $name, $oldmail, $email, $address, $zip, $city, $radius, $siret, $iban);
				session(['user' => $email]);

				if ($type === "patient" || $type === "practitioner") {
					// Get coordinates from nominatim
					$nominatim = 'https://nominatim.openstreetmap.org/search?format=json&q=';
					$searchAddress = str_replace(' ', '+', $address).','.$zip.'+'.$city;
					$locSearch = $nominatim.$searchAddress;

					$options = array(
						"http"=>array(
							"header"=>"User-Agent: StevesCleverAddressScript 3.7.6\r\n"
						)
					);

					$context = stream_context_create($options);

					$result = file_get_contents($locSearch, false, $context);

					if ($result !== false) {
						$loc = json_decode($result);

						$lat = 0;
						$lon = 0;
						if (count($loc) > 0) {
							$lat = $loc[0]->lat;
							$lon = $loc[0]->lon;
						}
						// Update coordiantes in DB
						updateLoc($accountType, $email, $lat, $lon);
					}
				}

//				redirect toward appropriate page
				if ($type === 'patient')
					return redirect()->to('/espace-patient/profil')->send();
				if ($type === 'practitioner')
					return redirect()->to('/espace-praticien/profil')->send();
				if ($type === 'admin')
					return redirect()->to('/espace-admin/profil')->send();
				}
			else {
				return view('error', ['errors' => $errorMsg]);
			}
		}
	}

	public function sendMessage() {
		//retrieve data contact
		if ($_SERVER['REQUEST_METHOD'] === 'POST'){
			$name = Input::get('name');
			$firstName = Input::get('first-name');
			$email = Input::get('email');
			$sjt = Input::get('subject');
			$msg = Input::get('msg');

			$name = filter_var($name, FILTER_SANITIZE_STRING);
			$firstName = filter_var($firstName, FILTER_SANITIZE_STRING);
			$email = filter_var($email, FILTER_SANITIZE_STRING);
			$sjt = filter_var($sjt, FILTER_SANITIZE_STRING);
			$msg = filter_var($msg, FILTER_SANITIZE_STRING);


			// Initializing variables
			$errorMsg = [];
			$errors = 0;

			//Verifiate Inputs
			if (!preg_match('/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i', $email)) {
				$errors++;
				array_push($errorMsg, 'Votre adresse email n\'est pas valide.');
			}
			if (!preg_match('/^(?=.*[A-Z])(?=.{2,})/i', $firstName) ||
				strlen($firstName) > 50) {
				$errors++;
				array_push($errorMsg, 'Prénom invalide');
			}
			if (!preg_match('/^(?=.*[A-Z])(?=.{2,})/i', $name) ||
				strlen($name) > 50) {
				$errors++;
				array_push($errorMsg, 'Nom invalide');
			}
			if (strlen($sjt) < 3) {
				$errors++;
				array_push($errorMsg, 'Sujet invalide');
			}
			if (strlen($msg) < 10) {
				$errors++;
				array_push($errorMsg, 'Message invalide');
			}

			if ($errors === 0) {


			//parts for boreale mail

			$baseMail = getMail('contact');
			$message = $baseMail['message'];
			$message = '<html><body>'.$message.'</body></html>';

			$message = wordwrap($message, 70);
			$subject = $baseMail['object'];

			$headers = "From: " .'cantin.p@codeur.online'. "\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

			//parts for user mail
			$bmail="cantin.p@codeur.online";

			$bmessage = '<html><body>
										<p>'.$msg.'</p>
									</body></html>';

			$bmessage = wordwrap($bmessage, 70);

			$bheaders = "From: " . $email . "\r\n";
			$bheaders .= "MIME-Version: 1.0\r\n";
			$bheaders .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

			//send mails
			mail($bmail, $sjt, $bmessage, $bheaders);
			mail($email, $subject, $message, $headers);

			//creat validation popup
			$popup = '<div id="overlay">
										<div id="popup">
											<p>Un mail de validation vous a &eacute;t&eacute; envoy&eacute;.</p>
											<a href="."><button>Retour à l\'accueil</button></a>
										</div>
									</div>';

			return redirect()->to('/')->send();
			}
			else {
				return view('error', ['errors' => $errorMsg]);
			}
		}
	}

	public function createAccount() {

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			// retrieve data
			$accountType = Input::get('account-type');
			$mail = Input::get('mail');
			$password = Input::get('password');
			$cpassword = Input::get('confirm-password');
			$fname = Input::get('firstname');
			$lname = Input::get('lastname');
			$address = Input::get('address');
			$zcode = Input::get('zip-code');
			$city = Input::get('city');

			// sanitize data
			$accountType = filter_var($accountType, FILTER_SANITIZE_STRING);
			$mail = filter_var($mail, FILTER_SANITIZE_STRING);
			$password = filter_var($password);
			$cpassword =  filter_var($cpassword);
			$fname = filter_var($fname, FILTER_SANITIZE_STRING);
			$lname = filter_var($lname, FILTER_SANITIZE_STRING);
			$address = filter_var($address, FILTER_SANITIZE_STRING);
			$zcode = filter_var($zcode, FILTER_SANITIZE_STRING);
			$city = filter_var($city, FILTER_SANITIZE_STRING);

			// Initializing variables
			$errorMsg = [];
			$errors = 0;

			// Data validation
			if ($accountType !== 'patient' && $accountType !== 'practitioner') {
				$errors++;
				array_push($errorMsg, 'Le type de compte ne peut être que patient
					ou praticien.');
			}
			if (!preg_match('/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i', $mail)) {
				$errors++;
				array_push($errorMsg, 'Votre adresse email n\'est pas valide.');
			}
			else {
				$exist = exist($mail);
				if ($exist === true) {
					$errors++;
					array_push($errorMsg, 'L\adresse mail saisie existe déjà');
				}
			}
			if ($password !== $cpassword) {
				$errors++;
				array_push($errorMsg, 'Vos mots de passe ne sont pas identiques.');
			}
			if (!preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$-.:-?{-~!"^_`\[\]\/])(?=.{8,})/',
				$cpassword)) {
				$errors++;
				array_push($errorMsg, 'Votre mot de passe doit contenir au moins 8
					caractères, dont une majuscule, une minuscule, un chiffre et un
					symbole.');
			}
			if (!preg_match('/^(?=.*[A-Z])(?=.{2,})/i', $fname) ||
				strlen($fname) > 50) {
				$errors++;
				array_push($errorMsg, 'Prénom invalide');
			}
			if (!preg_match('/^(?=.*[A-Z])(?=.{2,})/i', $lname) ||
				strlen($lname) > 50) {
				$errors++;
				array_push($errorMsg, 'Nom invalide');
			}
			if (!preg_match('/^(?=.*[A-Z])(?=.{2,})/i', $address)) {
				$errors++;
				array_push($errorMsg, 'Format d\'adresse invalide');
			}
			if (strlen($zcode) !== 5) {
				$errors++;
				array_push($errorMsg, 'Votre code postal doit contenir 5 caractères.');
			}
			if (strlen($city) < 2) {
				$errors++;
				array_push($errorMsg, 'Nom de ville invalide');
			}
			if ($errors === 0) {

				// Hash password
				$password = password_hash($password, PASSWORD_BCRYPT);

				// Insert new account into DB
				$random = generateLink();
				insertAccount($accountType, $mail, $password, $fname, $lname, $random, $address, $zcode, $city);

				// Get coordinates from nominatim
				$nominatim = 'https://nominatim.openstreetmap.org/search?format=json&q=';
				$searchAddress = str_replace(' ', '+', $address).','.$zcode.'+'.$city;
				$locSearch = $nominatim.$searchAddress;

				$options = array(
					"http"=>array(
						"header"=>"User-Agent: StevesCleverAddressScript 3.7.6\r\n"
					)
				);

				$context = stream_context_create($options);

				$result = file_get_contents($locSearch, false, $context);

				if ($result !== false) {
					$loc = json_decode($result);

					if (count($loc) > 0) {
						$lat = $loc[0]->lat;
						$lon = $loc[0]->lon;
						// Update coordiantes in DB
						updateLoc($accountType, $mail, $lat, $lon);
					}
				}

				$mailType = $accountType.'-validation';  //get appropriated mail
				$baseMail = getMail($mailType);
				$message = $baseMail['message'];

				$link = './confirmation/'.$random;

				$message = '<html><body>'.$message
										.'<a href="'.$link.'">
												<button style="padding:6px 10px;font-size:18px;color:#EEE;background-color:#864;">
												Confirmer le compte</button>
											</a>
										</body></html>';

				$message = wordwrap($message, 70);
				$subject = $baseMail['object'];

				$headers = "From: " .'cantin.p@codeur.online'. "\r\n";
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

				mail($mail, $subject, $message, $headers);
				$popup = 'un mail de confirmation vous a été envoyé.';
				return view('signup', ['popup' => $popup]);
			}
			return view('error', ['errors' => $errorMsg]);
		}
		$popup = 'une erreur est survenue';
		return view('signup', ['popup' => $popup]);
	}

	public function validateAccount($link) {

		// get email account
		validate($link);

		//redirect to home page
		return redirect()->to('/')->send();

	}

}
